package ru.t1.karimov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
