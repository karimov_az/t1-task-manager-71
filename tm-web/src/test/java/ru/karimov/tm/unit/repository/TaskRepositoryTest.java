package ru.karimov.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.config.WebApplicationConfiguration;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.util.UserUtil;

import static org.junit.Assert.*;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { WebApplicationConfiguration.class })
public class TaskRepositoryTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USER_PASSWORD = "test";

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final Task task1 = new Task("Task Test 1");

    @NotNull
    private final Task task2 = new Task("Task Test 2");

    @NotNull
    private final Task task3 = new Task("Task Test 3");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
        taskRepository.save(task3);
    }

    @After
    public void clean() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void deleteAllByUserIdTest() {
        final int expectedSize = 0;
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        assertEquals(expectedSize, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdAndIdTest() {
        @NotNull final String expectedId = task1.getId();
        @Nullable final String userId = UserUtil.getUserId();
        assertNotNull(userId);
        taskRepository.deleteByUserIdAndId(expectedId, userId);
        assertNull(taskRepository.findByUserIdAndId(expectedId, userId));
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        final int expectedSize = 3;
        assertEquals(expectedSize, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndIdTest() {
        @NotNull final String id = task1.getId();
        assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), id));
    }

}
