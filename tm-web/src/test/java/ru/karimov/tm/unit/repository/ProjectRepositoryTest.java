package ru.karimov.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.config.WebApplicationConfiguration;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.util.UserUtil;

import java.util.List;

import static org.junit.Assert.*;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { WebApplicationConfiguration.class })
public class ProjectRepositoryTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USER_PASSWORD = "test";

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    private final Project project1 = new Project("Project Test 1");

    @NotNull
    private final Project project2 = new Project("Project Test 2");

    @NotNull
    private final Project project3 = new Project("Project Test 3");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
        projectRepository.save(project3);
    }

    @After
    public void clean() {
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void deleteAllByUserIdTest() {
        final int expectedSize = 0;
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
        assertEquals(expectedSize, projectRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdAndIdTest() {
        @NotNull final String expectedId = project1.getId();
        @Nullable final String userId = UserUtil.getUserId();
        assertNotNull(userId);
        projectRepository.deleteByUserIdAndId(userId, expectedId);
        assertNull(projectRepository.findByUserIdAndId(userId, expectedId));
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        final int expectedSize = 3;
        @NotNull final List<Project> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        assertNotNull(projects);
        assertEquals(expectedSize, projects.size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndIdTest() {
        assertNotNull(projectRepository.findByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

}
