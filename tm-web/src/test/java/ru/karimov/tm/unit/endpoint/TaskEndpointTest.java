package ru.karimov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.config.DatabaseConfiguration;
import ru.t1.karimov.tm.config.WebApplicationConfiguration;
import ru.t1.karimov.tm.model.Task;

import javax.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ContextConfiguration(classes = { WebApplicationConfiguration.class, DatabaseConfiguration.class })
public class TaskEndpointTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USER_PASSWORD = "test";

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private final Task task1 = new Task("Task Test 1");

    @NotNull
    private final Task task2 = new Task("Task Test 2");

    @NotNull
    private final Task task3 = new Task("Task Test 3");

    @NotNull
    private final Task task4 = new Task("Task Test 4");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        add(task1);
        add(task2);
    }

    @SneakyThrows
    private void add(@NotNull final Task task) {
        @NotNull final String url = TASK_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private List<Task> findAll() {
        @NotNull final String url = TASK_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));
    }

    @SneakyThrows
    private Task findById(@NotNull final String id) {
        @NotNull final String url = TASK_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Task.class);
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = TASK_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task2);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        assertNull(findById(task2.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        @NotNull final String url = TASK_URL + "deleteById/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        assertNull(findById(task1.getId()));
    }

    @Test
    public void findAllTest() {
        final int expectedSize = 2;
        assertEquals(expectedSize, findAll().size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Task task = findById(task1.getId());
        assertNotNull(task);
        assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void saveTest() {
        final int expectedSize = 3;
        add(task3);
        @Nullable final Task task = findById(task3.getId());
        assertNotNull(task);
        assertEquals(expectedSize, findAll().size());
        assertEquals(task3.getId(), task.getId());
    }

}
