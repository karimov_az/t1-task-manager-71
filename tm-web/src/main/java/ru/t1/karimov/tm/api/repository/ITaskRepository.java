package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

    void deleteAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    @NotNull
    List<Task> findAllByUserId(String userId);

    @Nullable
    Task findByUserIdAndId(String userId, String id);

}
