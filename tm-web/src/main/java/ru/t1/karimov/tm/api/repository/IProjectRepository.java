package ru.t1.karimov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.model.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

    void deleteAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    @NotNull
    List<Project> findAllByUserId(String userId);

    @Nullable
    Project findByUserIdAndId(String userId, String id);

}
