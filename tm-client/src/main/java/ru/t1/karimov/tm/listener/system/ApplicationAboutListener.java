package ru.t1.karimov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.karimov.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.karimov.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(request);
        System.out.println("name: " + response.getName());
        System.out.println("e-mail: " + response.getEmail());
    }

}
